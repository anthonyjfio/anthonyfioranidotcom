### Reading Log ###

Books marked with one asterisk (*) are recommended.

Books marked with two asterisks (**) are highly recommended.

2015

+ *[The Hybrid Athlete]("http://www.store.jtsstrength.com/resources/hybrid-athlete")* by Alex Viada**
+ *[Total Money Makeover](http://amzn.to/1NpynrW)* by Dave Ramsey
+ *[10% Happier](http://amzn.to/1vQTQ2P)* by Dan Harris**
+ *[On The Shortness Of Life](http://amzn.to/1BxeCtI)* by Seneca*
+ *[Managing Oneself](http://amzn.to/14ExcUS)* by Peter Drucker**

2014

+ *[Jesus' Son: Stories](http://amzn.to/14cfcjn)* by Denis Johnson*
+ *[I Will Teach You To Be Rich](http://amzn.to/1DHEClM)* by Ramit Sethi**
+ *[Vagabonding](http://amzn.to/1xZxAdj)* by Rolf Potts*
+ *[The War of Art](http://amzn.to/1I4HfR3)* by Steven Pressfield**
+ *[The Art of Learning](http://amzn.to/1ATYen4)* by Josh Waitzkin
+ *[The Way of the Superior Man](http://amzn.to/1yMCcDf)* by David Deida**
