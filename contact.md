---
title: Contact
---

### Contact

**Email:** <a href="mailto:anthony@anthonyfiorani.com">anthony@anthonyfiorani.com</a>

**Phone:** I have a cell phone but it's not that easy to get my number

**Twitter:** <a href="https://www.twitter.com/_anthonyfiorani">@_anthonyfiorani</a>
