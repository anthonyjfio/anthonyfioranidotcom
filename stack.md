### Stack

##### [Local Machine](http://amzn.to/1zUzABf)
* [Arch Linux](https://www.archlinux.org/)
* [Xmonad](http://xmonad.org/)
* [Terminology](https://www.enlightenment.org/p.php?p=about/terminology)
* [Zsh](http://www.zsh.org/)
* [Vim](http://www.vim.org/)
    * [Vim Plug](https://github.com/junegunn/vim-plug)
        * [Syntastic](http://vimawesome.com/plugin/syntastic)
        * [Nerd Tree](http://vimawesome.com/plugin/the-nerd-tree)
        * [Fugitive](http://vimawesome.com/plugin/fugitive-vim)
* [Hakyll](http://jaspervdj.be/hakyll/)
* [Gitlab](https://about.gitlab.com/gitlab-com/)

##### [Remote Machine](https://www.linode.com/?r=0d82dffb31a608aced2dead7ba60827893fb4861)
* [Arch Linux](https://www.archlinux.org/)
* [Nginx](http://nginx.org/)
