---
title: The Work
date: 2014-11-01
---

We were trained by society to dislike the work we do. Homework was given to students as punishment. Parents give their kids extra chores to try to teach them a lesson. Then we seek out rest by shutting off in front of a glowing TV or computer screen.

Jeff Bezos in *The Everything Store* says something along the lines of "We're here to work." I beleive there is a lot to that.

I'm not saying that our entire life should be devoted to work, but we should enjoy the work we do. And more often than not we should probably be doing more work. It doesn't have to be at your job, and it probably shouldn't be unless that's your passion. But take time and work on developing some drawing skills, learn to make a website, something like that. Put some work into it and learn some things along the way, if you like it stick to it if not find another skill to work on.

Everyone is born with a natural desire to do things. Young children are good examples of this, they're always running around wanting to do things. But then we sit them in a classroom and tell them to sit down and shut up. I belive this dampens our desire to work.

If you learn to enjoy the work that you do it makes your day much better and it makes getting things done much easier. In *The War Of Art* Steven Pressfield talks about the concept of resistance in people lives. The way it's described is resistance is what keeps you on the couch watching TV and eating doritos. But overcoming resistance is a skill itself, it takes courage to sit down and start doing the work.

Start small with adding work to your day, and make it something you enjoy. Tim Ferriss is a writer that has written a few New York Times Bestsellers and when you listen to him desicribe his writing process he mentions how difficult it is. Then he goes on to mention that he would aim to write two crappy pages a day. Doing something with that consistency will help you overcome resistance and you'll build momentum towards what you're working for.
