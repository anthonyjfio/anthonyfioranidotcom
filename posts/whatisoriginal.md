---
title: What is Original?
date: 2015-03-04
---

Our minds are constantly thinking all day. Coming up with new ideas and different thoughts. Sometimes there's even a feeling or two sprinkled in there.

But my amatuer attempt at math is in. There's roughly 7 billion people on this planet. That's more seconds than there are in a whole year.

This means that your if your mind were to come up with one new thought every second of a whole year it probably wouldn't be a unique thought. There are all those people out there thinking similar things to you. 

If you really think you have a unique thought, [search for it](https://duckduckgo.com/). Chances are there's someone out there that has written about it already. But then again you have to think about how small of a percentage of the global population is writing on the internet. If you ask me it's probably pretty small, so it is also possible that it has been thought before but not written.

I think there is something out there that seperates some people from the rest.

It's taking action on your ideas.

I like to follow the 3% rule. Like most other things it's not a unique idea, I just don't remember where I heard it. 

This rule states that if you share an idea, only 3% of people will take action on it. The other 97% will just file it away somewhere in there head.

But this brings us back to the title. 

*What is Original?*

This is not a question of ideas. It's instead a question of action.

Have you taken action on your ideas?

Have you created something with your ideas?
