---
title: Learn to Code
date: 2014-11-30
---

A few days ago I watched an [interview with Steve Jobs](http://www.imdb.com/title/tt2104994/) while I was walking on the treadmill. 

It was full of information and unique ideas that Jobs had but there was one thing that really stuck out to me. At one point he mentions that he thinks every person should learn to program a computer at one point in there life because it teaches you how to think.

This motivated me even more on my [Code Academy](http://www.codecademy.com/) journey. Then when I was reflecting on the things that I have learned from HTML these things stuck out

1. **Everything you start has to be finished.** In HTML you use DIVS, SPANS, and all sorts of other tags but everything you start has to be finished. See example below.

		<!DOCTYPE html>
		<html>
		<head>
			<title>Sample Title</title>
		</head>
		<body>
			<p>Sample Paragraph</p>
		</body>
		</html>


2. **It doesn't have to be perfect.** When you run my websites home page through this [checker](http://validator.w3.org/check?uri=anthonyfiorani.com&charset=%28detect+automatically%29&doctype=Inline&group=0) you get two errors even though my website has no problem showing up in browsers. Even [Google](http://validator.w3.org/check?uri=google.com&charset=%28detect+automatically%29&doctype=Inline&group=0&user-agent=W3C_Validator%2F1.3+http%3A%2F%2Fvalidator.w3.org%2Fservices) has error (twenty-eight to be exact).

For now the only programming language I somewhat know is HTML but eventually I'd like to get to other languages. If you know how to code [email me](mailto:anthony@anthonyfiorani.com) and tell me how it has taught you to think.
