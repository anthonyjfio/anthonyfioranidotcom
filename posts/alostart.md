---
title: A Lost Art
date: 2014-10-09
---

Most people have at least a few things in their life that they wish to change or improve on. Maybe it's losing weight or perhaps be more productive.

But when it comes to those things it seems that they look for the easy way out. I know because this was me. I was the one trying crazy diets just because I didn't want to track my calories. At one point I was doing intermitent fasting regularly and I lost weight but a large portion of the weight lost was muscle. I've also tried a ketogenic diet, which definitely helps you lose weight but it made me feel horrible. Don't even get me started on the methods I used ot cut weight when I was wrestling, those were the worst out of everything I have done.

I did all that just to avoid tracking my caloric intake, and truth be told tracking your calories really isn't that hard. After a while it just becomes a habit, and [MyFitnessPal](http://www.myfitnesspal.com/) makes it incredibly easy. All you have to do is scan the barcode on what you're eating and select a serving size.

This doesn't just apply to food, it applies to whatever you can think of. For tracking/limiting my time on the internet I've been using [StayFocusd](https://chrome.google.com/webstore/detail/stayfocusd/laankejkbhbdhmipfmgcngdelahlfoji?hl=en) for my browser, [Stay Focused](https://play.google.com/store/apps/details?id=com.stayfocus&hl=en) for my phone and [Rescue Time](https://www.rescuetime.com/) for overall tracking.

On top of all that I have been attempting to cut down on the distractions my phone creates. Which means deleting apps like youtube and chrome. Basically uninstalling or disabaling everything that I felt could distract me from whatever it is I'm working on.

There are all types of applications out there to help you track things, you could even go old school and use pen and paper which is what I use for my workout log. <a href="mailto:anthony@anthonyfiorani.com">Email</a> to let me know what you're tracking and how you're doing it.
