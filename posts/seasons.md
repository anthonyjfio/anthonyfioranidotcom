---
title: Seasons of Life
date: 2015-04-07
---

A pretty close friend of mine was having a rough day last weekend.

He was mentioning how he keeps working and working and doesn't feel like he's getting anywhere. He also mentioned how he is upset that he still doesn't have a woman in his life.

So of couse I listened to everything else he had to say. But later in the night I mentioned the seasons of life to him.

As most other things I have forgotten where I heard this but basically it's a theory that states your life goes to phases(seasons).

Usually it starts with winter. In this phase you're learning, and reading. This can be compared to how farmers spend their winters fixing their equipment and prepping it for spring.

Then spring comes, this is where you're starting projects or relationships. For farmers this means planting crops.

In the summer you tend to your projects and relationships, feeding them and helping them grow.

Then fall is when the rewards for your hard work come. This is when you earn the most and are happy about the relationships in your life. For the farmer this would be the time your harvest all your crops.

This can be applied to many things in life. Some things will be further along than others. Sometimes you may even restart the cycle, but just know that your work will be rewarded eventually.
