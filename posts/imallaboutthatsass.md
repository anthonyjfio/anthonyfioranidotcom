---
title: I'm All About That Sass
date: 2015-01-08
---

No I'm not talking about that overplayed [song](https://www.youtube.com/watch?v=7PCkvCPvDXk). 

I'm talking about a css preprocessor named [Sass](http://sass-lang.com/). 

A what?

A preprocessor, basically what it does is let you create a document in a different language that's usually more simple than the language it outputs. Here's a simple equation to help you understand.

> Hi -> preprocessor outputs -> Hello

If you were paying attention you would have noticed that I said the input is usually more simple than the output. With the input being two letters and the output being five, and the meaning being roughly the same I'd say this is a good example of what Sass does.

The difference is that Sass does this with a language used to style websites called CSS.

This is what CSS looks like.

	body {
	  color: #484848;
	  font-family: Helvetica;
	  background-color: #fff;
	 }
	a:visited {
	  color: #3b3b3b;
	}

If we were to use the Sass preprocessor we could have saved ourselves some work just by installing Sass and learning a language extremely similar to that what we already know.

Here's an example of what you would have to input to Sass to get the same CSS as above.

	body 
	  color: #484848
	  font-family: Helvetica
	  background-color: #fff
	a:visited
	  color: #3b3b3b

  So with Sass we don't have to enter the semicolons or curly brackets, and it uses tabs to seperate different selectors. Over the course of a whole project this could save a lot of typing and there's also all kinds of features Sass has that CSS doesn't. But I haven't learned all those features yet so stay tuned for more adventures in website development and programming.
