---
title: The Building Process
date: 2014-10-24
---

When I was younger I was constantly building things with Lego's and whatnot. Then as I moved through middle school and high school I feel like I lost some of that creativity, only really branching out to do the occasional woodworking project. But now I'm trying to get back to my Lego days.

So I decided to build a website, from scratch. I'm not using lame tumblr or wordpress. With building it from scratch I can change anything I want to however I want. It is more difficult and requires more learning but learning is a good thing. It keeps the yang up.

Building something from scratch is difficult but the feeling you get when it starts to take shape is incredible. The reason I say "take shape" is because I don't think my website will ever be finished, it'll always be a work in progress.

Building something to me isn't about the finished product it's about the process. It's about the traits you develop going through the process.

On a sidenote my next projects are a cool [hamburger menu](http://codepen.io/swirlycheetah/full/cFtzb), learning some [haskell](http://www.haskell.org/haskellwiki/Haskell) to better understand the inner workings of [hakyll](http://jaspervdj.be/hakyll/) and some poetry. Why poetry? I don't know, I've just had some weird itch to learn how to write it lately.
