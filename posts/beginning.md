---
title: The Beginning
date: 2014-10-17
---

For a rather long time I knew about the benefits of meditation put put it off because I thought it was a waste of time.

Then I opted for more technical ways of meditating like using an [emWave2](http://store.heartmath.org/emWave2/emWave2-handheld). Now that definitely helped me and gave me some of the benefits of meditation. But I do not feel that it can replace it, although it can be used in addition to meditation.

Anyway two weeks ago I decided to start meditating consistently. So I wrote "consecutive days I've meditated," on the whiteboard in my room and I'm keeping track by placing tally marks under it. This is keeping me accountable.

My meditating usually involves me sitting on the floor with my legs crossed and back against my bed for ten minutes. Eventually I would like to work my way up to twenty minutes but I figured ten was a good start.

So far building this habit has improved my mood and motivation to get things done. But the most profound thing that I have found with this is that several other habits that I've been wanting to build for myself have kind of just fallen into place. 

Why is this? I'm not sure, but I'm reading more often and socializing more with friends, family and classmates. 

I've heard some people refer to meditation as working out for your brain I agree with this. 

I encourage all of you attempt meditating for at least two weeks straight and [email](mailto:anthony@anthonyfiorani.com) about your experience with it.
