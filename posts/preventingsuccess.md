---
title: Preventing Success
date: 2015-02-13
---

Earlier this week I heard a very smart person say that people are always looking at what they can do to become more successfull, but they never look at what they're doing to prevent their success.

This really resonated with me. For example if you start lifting weights and immediately start looking at better ways to set up your routine without ever correcting your form you're on a path to failure. Chances are you'll make progress for a while then you'll hurt yourself, thus limiting your progress. Whereas if your were to start with correcting your form you'll ensure there's *less* of a chance you'll get injured. 

The reason I say less is because from what I've learned there's no such thing as 100% freak accidents that are out of your control do happen. 

Since I've heard this I've really started to analyze my daily habits and tried to find which habits of mine are holding me back.
