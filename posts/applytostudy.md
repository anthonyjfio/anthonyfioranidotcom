---
title: Small Excerpt: Apply Yourself to Study
date: 2015-02-15
---

While I was reading [On The Shortness of Life](http://amzn.to/1BxeCtI) today a sentence really stood out to me, so I wanted to share it.

> If you apply yourself to study you will avoid all boredom with life, you will not long for night because you are sick of daylight, you will be neither a burden to yourself nor useless to others, you will attract many to become your friends and the finest people will flock about you. - Seneca

[Email me](mailto:anthony@anthonyfiorani.com) and tell me what you think.
