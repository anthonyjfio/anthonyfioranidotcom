---
title: Is This Necessary?
date: 2015-03-17
---

So friends, my weekend was rather uneventful. Except for Friday night.

What happened was I went on a rather bad date. She had to leave early so I just decided to walk around town for a bit because it was actally semi-warm here in Michigan.

While I was walking to my car I saw a couple saying their goodbyes next to her car. I think me walking by embarresed them, but I laughed about it once I sat down in my car.

But as I was driving towards the main road I swore that I saw one of my ex-girlfriends. Immediately my stomach drops.

By the time I get home I can't help but feel my stomach turning and I kept thinking about her.

But then I thought to myself, is this useful? Are these thoughts useful? Is my stomach turning useful?

No.

So I try to be mindful of them, notice where they are in my body(my stomach) and then bring my focus back to my breathe. Surely enough those thoughts disappear.

This was by no means an [original idea](../whatisoriginal). I've been reading [10% Happier](http://amzn.to/1vQTQ2P) and in the book the author mentions something one of his teachers said about him having trouble meditating. That was something along the lines of "is that thought useful to you in the moment?"
