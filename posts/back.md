---
title: First Post Since I've Gone Missing
date: 2014-10-16
---

For what seems like forever my website was down due to me switching hosting and learning to use hakyll.

So far learning hakyll is pretty exciting. I think that it will save me a lot of time and as a result I should be able to get more material out to you.

If you don't know what hakyll is it's a static website generator. So it creates a website from a template HTML file and plain text files. The plain text files can be anything from your posts to your about page.

So for example if I wanted add another button to the navigation bar at the top of your screen all I would have to do is update the template file. Where as before I would have to update every webpage individually.

Like I said before this should save me quite a bit of time, and hopefully I'll be able to get a little more creative with the template file.
