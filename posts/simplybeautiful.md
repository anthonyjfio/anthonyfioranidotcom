---
title: Simply Beautiful
date: 2014-10-22
---

Over the past few months I have been attempting to simplify my life by [getting rid of excess items](getridofyourstuff.html). One day I hope to get down to one hundred total personal items. But as of right now that will take a rather long time because I have to much to count.

But an unexpected benefit of all the simplifying I have been doing is that I don't need to do much cleaning anymore. I do live with my parents so the cleaning that I do is limited but my room has never been cleaner. I no longer have dresser drawers packed full of junk, in fact most of them are empty now.

On top of that I'm seeing the beauty in less. It's so clean, so simple.

I'm continuing on my journey of simplicity but what I wish to concour next is mental simplicity via [meditation](beginning.html) and getting rid of distractions.

So hopefully one day I'll have a beautiful home and mind.
