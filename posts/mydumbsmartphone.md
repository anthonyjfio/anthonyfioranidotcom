---
title: My Dumb Smartphone
date: 2014-12-21
---

I'd like to start with a quick story of when I went to the comedy club a couple weeks ago. 

One of the comedians was commenting on how technology has changed and how things used to be. He points out that when he was younger the only phones that existed were attached to the wall and all they did was allow you to call people. But if people back then acted like they do now with cell phones they'd be going back to the wall the phone is on every two minutes to see if it did something cool.

I promise it was funnier in person. Although this did get me thinking more about what I'm allowing on my phone.

There are many advantages to having a smartphone that I see. Such as the navigation capabilities, music, text messaging, and phone calls of course.

But then there are an incredible amount of time wasting activities that you can do on your phone. Including but not limited to browsing internet, Twitter, Facebook, Snapchat and Candy Crush.

All these methods of communication, tools and forms of entertainment can be too much at least for me. There are so many cool apps out there and I wanted all of them. But then I started to notice that a large portion of my day was spent looking at my screen wasting time.

So how did I fix it? By eliminating the distractions.

I did this by uninstalling or disabling as much as possible and only use the things that are productive or are non-time wasters. So the uninstalled apps included email, all social networking, extra forms of communication and un-used apps. I also decided to limit myself to two types of communication on my phone because anymore than that just overwhelms me, sometimes this is even too much. The types of communication I choose were phone calls and text messaging.

Text messaging is another monster though, it can also take up a large portion of your day if you let it. It seems as if for every one message you send you recieve two in return. Lately I've been trying to limit it by simply calling people instead and it seems to work. But occasionally there are some things that are easier to communicate through text.

Disclaimer: I'm not saying a dumb smartphone is for everyone, but if you're spending a large portion of your day on it like I was you should consider it. For tracking how much time you spend on your phone or computer check out [Rescue Time](https://www.rescuetime.com/).
