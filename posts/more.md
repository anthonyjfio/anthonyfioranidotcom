---
title: There's Always More
date: 2015-04-30
---

I hate to say it but I feel like I came upon a revelation today while I was meditating.

I realized that there will always be more things to fix about myself, more items on the todo list, more books to read, more things to make, more meditating to do and more things to break.

Nothing will ever be perfect, there will always be something about it to change.

I know that I've heard all this before but I feel that it means more to me now that I've come to the realization myself.

For me the more important thing I realized from this is that I should stop trying to consume so much information. This information isn't necessarily bad it's just clouding my thinking. I can always come back to it.

But I feel like my mindset is changing with the seasons. It's time for me to cut more things out of my life.
