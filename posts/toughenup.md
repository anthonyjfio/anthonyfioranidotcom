---
title: Toughen Up
date: 2014-11-04
---

Most people could toughen up a little bit, I know I could. All the modern amenities available today make life much easier than it has been in the past. Chances are if you were to lose all your money and assets you could find a place to sleep, eat, and bathe. Yet most of modern society lives in houses much bigger than what they need with a lot more [stuff](getridofyourstuff.html) than what is necessary. 

So what I'm proposing is that we toughen up a bit. Go camping for a weekend and leave all your modern amenities behind. Go without food until dinner for a day. You could even attempt to run a couple miles.

What this will do is provide you with a contrast bias. 

Say you don't eat until six in the afternoon one day. How much better does that food you eat at dinner taste?

I guarantee it tastes incredible, much better than usual. This is because you appreciate it much more. You went without food for twenty-four hours or so. 

This could create a new found appreciation for all modern amenities. You'll be much more thankful for a chair after running three miles, and after spending a weekend in a tent sleeping on a tent a bed will feel like heaven.

Back when I first started wrestling I went to a wrestling camp that was meant for high school wrestlers. But it was only my first year of wrestling and I just finished seventh grade. The camp was a week long and it was grueling, we had three sessions of wrestling practice per day and two conditioning sessions per day. Needless to say it was a very long week for me, but on the last full day of the camp we were told to run for two hours and we were expected to get ten to fifteen miles in that time. That run was extremely difficult for me, I ran twelve miles that day. But I can look back on that experience and it will provide a contrast to whatever I am experiencing now or in the future.

There are many ways you could do this to increase your toughness. I believe the first place I heard of this was one of [Tim Ferriss's](http://fourhourworkweek.com/blog/) comfort challenges in his book [4 Hour Work Week](http://www.amazon.com/The-4-Hour-Workweek-Anywhere-Expanded/dp/0307465357). [Email Me](mailto:anthony@anthonyfiorani.com) and tell me what ways you're going to try to toughen up.
