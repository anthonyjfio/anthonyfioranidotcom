---
title: Get Rid Of Your Stuff
date: 2014-09-03
---

Growing up my mother was somewhat of a hoarder, nothing against her it's just what she tends to do. Our attic would be stuffed full of stuff that wasn't touched for years at a time. Then when we moved out of my dads house we had boxes of stuff in the garage stacked to the ceiling. After one more move we ended up with a basement so full of stuff you could barely breathe.

So naturally my siblings and I picked up these tendencies. Collecting stuff and not using it for extended periods of time, or in some cases never using it. I think there is still a closet full of my childhood toys in my dads house.

Over the past month I have started reading [The Minimalists](http://www.theminimalists.com/), [Ev Bogue's](http://evbogue.com/) and [Leo Babauta's](http://zenhabits.net/) blogs. As a result of that I have been going through my room and attempting to get rid of anything that I haven't used in the last sixty days.

One of the things I decided to get rid of was an notebook that I haven't touched in ages. A few days later I come home to my mother complaining about how I threw away a good notebook. This shows you how attached some people get to stuff.

Anyway I've also been going through my clothes and deciding what I want to get rid of. The rule I was trying to use is if I haven't used it within the last sixty days get rid of it. I felt that some of my old wrestling shirts were exceptions to this, it might have been the fact that they validate me because of the first place on the back. But I'm sure with time I'll work up the courage to part with these items.

Once I had a few garbage bags full of clothes I looked up the nearest Salvation Army and headed there. I didn't really know what to expect because I had never donated anything before. But when I got there there were signs directing people with donations to drive around back and there were employees back there to unload my stuff before I could even help them. I was done and on my way home in less than two minutes.

So go pack up some of your stuff that you haven't used in sixty days and donate it to the nearest Salvation Army.

My reason for doing this is that in the future I wish to travel or maybe move to a new location, and I feel that all this excess stuff acts as an anchor holding me in my current state of mind and location. Your reasons may be different, but to see other peoples reasons for living this way I recommend you check out this TED talk done by [Adam Baker](https://www.youtube.com/watch?v=9XRPbFIN4lk&feature=youtu.be) or this one by the guys over at [The Minimalists](https://www.youtube.com/watch?v=GgBpyNsS-jU&feature=youtu.be).
