---
title: Stuff
date: 2014-12-09
---

How does one signal affluence to others?

Do you run around with your bank statement showing everyone? No.

Do you lease a brand new car? Don't do that, if you don't know why go read about [personal finance](http://amzn.to/1zw2Htx).

In modern society this is most commonly done through purchasing an excess of things (gadgets, gizmos, furniture, clothes, cars, and the like). Most of it is useless except for being good paperweights.

But why do we try to signal affluence? Well men especially do this to show dominance over other men and essentially become the *alpha* male, women do this too and I feel the reasons are similar. 

Money is the currency in this game we all play called *life* but bragging about it or show boating about is looked down upon. But having rooms full of stuff we don't use is commonplace.

I don't have any solutions to this because it's just the way most of society is and most likely will be for a long time. But I do have a proposal.

**We start collecting experiences instead of physical objects.**

Will it signal affluence? Probably, but collecting experiences such as travelling can be done for a much [lower price](http://snarkynomad.com/cost-of-budget-travel-for-a-month-in-guatemala/) than collecting things.
