---
title: All The Small Things
date: 2014-11-19
---

I've been reading [I Will Teach You To Be Rich](http://amzn.to/1zw2Htx) and in the beginning of the book he mentions how the majority of people love to deliberate the minutiae.

This really got me thinking. Most people not only deliberate over the small things they dwell on them. This could be anything from what you think the opposite sex thinks of the bump on your nose, to the amount of time you spent studying on that exam you have next week.

But honestly these small things don't make a big difference. I've found that cutting my study time a half hour short to get more sleep doesn't impact my grade at all. Then the giant bump on the bridge of my nose, yeah no girl has ever mentioned anything about it to me. It could be that they don't want to tell me about it but most likely they're to busy dwelling on their small things.

These things don't matter much, the impact that they have on your desired outcome is marginal. 

If you haven't already heard of [Pareto's principal](http://en.wikipedia.org/wiki/Pareto_principle), it states that 20 percent of your effort can create 80 percent of your results. For example in business 20 percent of their marketing will bring in 80 percent of the profits. It's not always exactly 80 and 20 percent, it could be 85 and 15 or 90 and 10 but you get the idea. This can be applied to  marketing, dating, fitness, and everything else.

These small things like I mentioned above are not your heavy hitters, so they're not the things you want to focus on.

The heavy hitters would be the 20 percent, so find the bang for your buck things to focus on and stick to those. Consistently execute them, then once in a while it's okay to dip into the smaller things but don't let them take up too much of your time.
