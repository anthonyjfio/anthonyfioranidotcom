---
title: Bliss is Being
date: 2014-11-13
---

Be in the present moment. Be in the room with the person you care about. Be at the dinner table with your food, and enjoy it. Be on the beach with the waves and the sun.

Don't think about what you're doing tommorrow, or what you forgot to do at work today. Be in the present moment. No thoughts of the future or past. Just be.

This is rather difficult to accomplish but meditation helps quite a bit. I'm not claiming to be a buddhist monk but now that I have been [meditating consistently](beginning.html) I'm able to experience just being.

I may be listening to to many people talk about meditation, but I heard someone say that *bliss is being*. I truly believe that, some of my most pleasent moments in the past month have been when I was meditating in a dark quite room by myself.

Now creating the habit of meditation or any habit is a whole different monster. It seems to be like working out for some people, they know they should do it but they experience a lot of resistance in forcing themselves to do it.
