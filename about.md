---
title: About
---

### About

+ I was born and raised a Michigander
+ I have been an athlete the majority of my life and I believe pushing yourself to the limit physically will change you as a person
+ I'm currently employed as an office worker, which is not my dream profession
+ I'm a student at the community college in my area currently pursuing a business management degree
+ I did attempt to buy a brand new car at one point, and it was my worst financial decision yet
+ I'm in the process of throwing away or selling as many of my possesions as possible in an effort to simplify my life
+ I will be moving out of the country as soon as I find a way to make money more consistently online
+ I created my website entirely by myself, no help from wordpress or square space
+ I have a six year old laptop with Arch Linux installed on it and use Xmonad as my window manager
+ I drive a 2001 Ford Focus with a manual transmission and I'm proud of it, don't like it? Then lets race
+ I'm a recovering fitness addict, yes that exists 

### Current Strength Levels

+ Squat - 330lbs
+ Bench - 245lbs
+ Deadlift - 500lbs
+ Overhead Press - 185lbs

### Buy Me Lunch

+ **Bitcoin:** 19sFvjCN9hUAcRCbE6toXNYGtjr1UzzntK

### What I Used To Make My Website

Note: Some of these are referal links

+ [GitLab](https://gitlab.com/)
+ [Namecheap](http://www.namecheap.com/?aff=73042)
+ [Linode](https://www.linode.com/?r=0d82dffb31a608aced2dead7ba60827893fb4861)
+ [Hakyll](http://jaspervdj.be/hakyll/)
